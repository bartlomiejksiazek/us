/*
 * main.c
 *
 * Created: 2014-11-05 21:51:00
 *  Author: 
 */ 


#include <stdio.h>

#include <avr/io.h>

#include "scheduler.h"


void switchLed(foo_params params) {
	PORTA = ~PORTA;
}

void init(){
	addTask(0, 1000, switchLed, NULL);
}


int main(void)
{
	init();
	execute();
    
}