#include "scheduler.h"

#include <stdio.h>

#include <avr/io.h>
#include <avr/interrupt.h>


#define MAX_NUMBER_OF_TASKS 5


struct Task taskArray[MAX_NUMBER_OF_TASKS];

ISR(TIMER0_COMP_vect){
	schedule();
}

void init_timer(){
	OCR0 = 250;
	TIMSK |=(1<<OCIE0);
	TCCR0 |=(1<<WGM01) | (1 << CS00) | (1 << CS01);
	sei();
}


void addTask(int priority, int period, foo_ptr foo, foo_params params) {
	struct Task * task = taskArray + priority;
	task->foo = foo;
	task->interval = period;
	task->ready = period;
	task->params = params;
	//for(int i=0;i<MAX_NUMBER_OF_TASKS;++i)
		//taskArray[i]=*task;
	}


void execute() {
	
	init();
	
	
	while(1){
		int k;

		if(wait==0) {
			cli();

			for(k = 0; k < MAX_NUMBER_OF_TASKS; k++){

				if (taskArray[k].ready == 0) {
					taskArray[k].foo(NULL);
					taskArray[k].ready = taskArray[k].interval;
				}

				if(k == MAX_NUMBER_OF_TASKS-1){
					wait = 1;
				}
			}

			sei();
		}
	}
	
	
	
}


void schedule() {
	int i;
	for(i = 0; i < MAX_NUMBER_OF_TASKS; ++i) {
		if (taskArray[i].ready > 0) {
			wait = 0;
			taskArray[i].ready--;
		}
	}
}
