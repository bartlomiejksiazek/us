#ifndef _SCHEDULER_H_
#define _SCHEDULER_H_


typedef void (*foo_ptr)(void *);
typedef void (*foo_params)(void *);

static int wait =0;

typedef struct Task {
	unsigned int interval;
	unsigned int ready;
	foo_ptr foo;
	foo_params params;
};

void addTask(int priority, int period, foo_ptr foo, foo_params params);

void schedule();

void execute();



#endif